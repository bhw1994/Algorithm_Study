import itertools

arr = []
resultTuple = ()
for i in range(9):
    arr.append(int(input().split()[0]))

comb = itertools.combinations(arr, 7)
for heights in comb:
    result = 0
    for height in heights:
        result += height
    if result == 100:
        resultTuple = heights
        break

resultTuple = sorted(resultTuple)
for i in resultTuple:
    print(i)
